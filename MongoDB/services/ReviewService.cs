﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Collections;

namespace MongoDB.services
{
    public partial class MongoDBService: IMongoDBService
    {
        public async Task<ReviewCollection> GetReviewByVersionAsync(string version) {

            var filter = Builders<ReviewCollection>.Filter.Eq(review => review.version, version);

            return await this._reviewCollection.Find(filter).FirstOrDefaultAsync().ConfigureAwait(false);
        }
    }
}
