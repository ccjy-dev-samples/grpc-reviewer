﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Collections;

namespace MongoDB.services
{
    public partial class MongoDBService: IMongoDBService
    {
        private readonly IMongoCollection<ReviewCollection> _reviewCollection;
        private readonly IMongoDatabase _mongoDatabase;

        public MongoDBService(IMongoDBSettings settings) {
            var client = new MongoClient(settings.ConnectionString);
            _mongoDatabase = client.GetDatabase(settings.DatabaseName);

            _reviewCollection = _mongoDatabase.GetCollection<ReviewCollection>(settings.ReviewCollection);

            CreateReviewCollectionIndexes();
        }

    }
}
