﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Collections;

namespace MongoDB.services
{
    public partial class MongoDBService
    {
        internal void CreateReviewCollectionIndexes()
        {
            var indexToVersionKeys = Builders<ReviewCollection>.IndexKeys.Descending(o => o.version);
            var indexToVersionModel = new CreateIndexModel<ReviewCollection>(indexToVersionKeys);

            this._reviewCollection.Indexes.CreateOne(indexToVersionModel);
        }
    }
}
