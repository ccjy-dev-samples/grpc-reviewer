﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Collections;

namespace MongoDB.services
{
    public interface IMongoDBService
    {
        Task<ReviewCollection> GetReviewByVersionAsync(string version); 
    }
}
