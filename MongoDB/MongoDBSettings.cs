﻿using System;

namespace MongoDB
{
    public interface IMongoDBSettings { 
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
        string ReviewCollection { get; set; }
    }
    public class MongoDBSettings: IMongoDBSettings
    {
       public string ConnectionString { get; set; }
       public string DatabaseName { get; set; }
       public string ReviewCollection { get; set; }
    }
}
