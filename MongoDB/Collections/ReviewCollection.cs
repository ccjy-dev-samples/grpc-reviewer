﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Models;

namespace MongoDB.Collections
{
    public class ReviewCollection: MongoDBIdentity
    {
        public string msg { get; set; }
        public string version { get; set; } // v1, v2, v3
    }
}
